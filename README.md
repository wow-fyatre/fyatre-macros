# Requirements
* WoW [Macro Toolkit](https://www.curseforge.com/wow/addons/macro-toolkit) (to allow longer macros)

# Offensive

### Multi-Shot Auto Target
Auto get target, even if not in combat.   
Picks a new target if current is dead or friendly   
```
#showtooltip Multi-Shot
/targetenemy [noexists][dead][help]
/use [exists]Multi-Shot
```

### Arcane Shot Auto Target
Auto get target, but only if in combat   
Picks a new target if current is dead or friendly   
```
#showtooltip
/targetenemy [noexists,combat][dead,combat][help,combat]
/use Arcane Shot
```

### Aimed Shot Auto Target
Auto targets enemy if in combat. Helpful for getting target immediately on pull   
Picks a new target if current is dead or friendly   
```
#showtooltip
/targetenemy [noexists,combat][dead,combat][help,combat]
/use Aimed Shot
```

### 1 min CDS + trinkets and optional potion
Keybind: Mouse numpad 11 (numpad -)   

```
#showtooltip
/cast [talent:6/3] Double Tap;
/use 13
/use 14
/use [mod:shift]Potion of Spectral Agility
```

### Big AOE
Keybind: Mouse numpad 5   

Resonating arrow followed by Volley (if talented)   
Shift forces Volley if needed and talented   
```
#showtooltip
/use [mod:shift,talent:7/3] Volley; 
/castsequence [talent:7/3,nomod] reset=30 Resonating Arrow, Volley
/use [notalent:7/3]Resonating Arrow
```

### Secondary AOE

Default: Rapid Fire   
Shift: Barrage or Explosive shot, if talented   
```
#showtooltip
/use [mod:shift,talent:2/2] Barrage;[mod:shift,talent:2/3]Explosive Shot; Rapid Fire
```

### 2 min CDS + trinkets
Includes some [commands](https://www.curseforge.com/wow/addons/macro-toolkit) to hide error sounds / text 

```
#showtooltip Trueshot
/mtsx
/use Trueshot
/use 14
/use 13
/mtce
/mtso
/use Kyrian Bell
/use [mod:shift]Potion of Spectral Agility
/use Oralius' Whispering Crystal
/use Red Firework
```

### Primal Rage
* Calls the correct pet   
* Casts Primal Rage if the correct pet is present   
```
#showtooltip Primal Rage
/use [nopet] Call Pet 1;
/use [pet:Webster/Disclosure]Primal Rage;
```

# Defensive

## Feign Death
Stop everything and feign death. Shadowmeld can make it appear as though I've released in PVP   

Default: Feign death followed by shadowmeld   
Shift: Shadowmeld   
Alt: Pet play dead   
```
#showtooltip
/stopcasting
/stopcasting
/cqs
/stopattack
/use [mod:shift] Shadowmeld
/castsequence [nomod:shift,nomod:alt]reset=29 Feign Death,Shadowmeld
/use [mod:alt,pet] Play Dead;
```

### Bounce
Disengage, with conditional Master's Call if correct pet is out (Bird)   
Shift forces Master's Call if correct pet is present.   
```
#showtooltip
/stopcasting
/stopcasting
/cqs
/use [pet:Ra,mod,@Fyatre] Master's Call;[nopet:Ra]Disengage
/castsequence [nomod,pet:Ra, @Fyatre]reset=5  Disengage, Master's Call
```

### OH SH*T button
Stop everything and protect self   

Default: Exhilaration + Healthstone / Phial (Does not use if full health)   
Shift: Aspect of the Turtle   
Alt: Cancel aspect of the turtle (stops potion/healthstone use)    
Ctrl: Healing potion   

```
#showtooltip [mod:shift][mod:alt]Aspect of the Turtle; [mod:ctrl] Spiritual Healing Potion; Exhilaration
/stopcasting
/stopcasting
/cqs
/cancelaura [mod:alt] Aspect of the Turtle
/use [mod:shift]Aspect of the Turtle;Exhilaration
/use [nomod:alt]Survival of the Fittest
/use [nomod:alt]Healthstone
/use [nomod:alt]Phial of Serenity
/use [mod:ctrl,nomod:alt]Spiritual Healing Potion
```

# Slow
Concussive shot, followed by web if spider is out.   
Shift forces web if spider is out   
Disable auto use of web by pet if timing is critical   
```
#showtooltip
/use [pet:Webster,mod] Web Spray;[nopet:Webster]Concussive Shot
/castsequence [pet:Webster, @target]reset=5 Concussive Shot,Web Spray
```

# Shoo
Stop everything a shoo enemy away   

Default: Bursting shot (knockback)   
Mod: Scare beast

```
#showtooltip
/stopcasting
/stopcasting
/cqs
/use [mod] Scare Beast;Bursting Shot
```





# Utility
### Mouseover Misdirection / Hunter's Mark
Keybind: F4   
```
#showtooltip
/use [@mouseover,help] Misdirection;[@mouseover,harm][] Hunter's Mark
```

### Focus target Misdirection / Hunter's Mark
Keybind: Mouse button 4   
```
#showtooltip
/use [mod:shift]Hunter's Mark;[@pet,mod,exists,nodead][@focus,exists,nodead][@pet,exists,nodead]Misdirection;[@mouseover,harm][]Hunter's Mark
```

### Pet control
Keybind: Middle Mouse + <mod>   

No pet:   
* Focus exists: Summon DPS pet
* No focus: Summon tank pet
* ctrl, shift, alt modifiers select specific pet

With pet:
* Heal pet + attack target
* Alt: Dismiss pet
* Shift: Move to position
* Ctrl: Pet follow (cancel move to)

Dead pet (visible):
* Revive pet (Only works if pet is visible, needs tweaking)

```
/petattack [nomod,pet][mod:ctrl,@mouseover,harm,pet]
/use [pet,mod:alt]Dismiss Pet;[nopet,mod:alt]Call Pet 1;[nopet,mod:shift]Call Pet 3;[@focus,exists,nodead,nomod,nopet]Call Pet 1;[nomod,nopet][mod:ctrl,nopet]Call Pet 2;[pet,dead][]Revive Pet
/petmoveto [pet,mod:shift]
/petfollow [pet,mod:ctrl]
```

### Mouseover pet control
Keybind: F2

If mouseover is enemy, send pet to attack it. Specific pets to use their ability.   
If mouseover is friendly, send pet to aid them with Master's Call (specific pets)   
If no mouseover exists, recall pet to follow.   

```
#showtooltip
/petfollow
/use [pet:Disclosure, @mouseover,harm] Dash
/use [pet:Ra,@mouseover,help] Master's Call(Cunning Ability);[pet:Ra,@mouseover,harm] Talon Rend;  
/use [pet:Ringo/Cronch,@mouseover,harm] Growl
/use [pet:Webster,@mouseover,harm] Web Spray

```


### Trap & Flare
Keybind: Mouse button 5   

Default: Tar trap followed by Binding Shot   
Shift: Freezing trap   
Ctrl: Explosive trap (PVP)   
Alt: Flare   

```
/use [mod:ctrl] Hi-Explosive Trap;[mod:shift] Freezing Trap; [mod:alt] Flare
/castsequence  [nomod] reset=25 Tar Trap, Binding Shot
```


### Engineer battle rez
Keybind: F3   
Mouseover, otherwise target if the target is dead   
```
/use [@mouseover,help,dead][@target,help,dead]Disposable Spectrophasic Reanimator 
```


# STFU (Silence / Dispel)
Stop everything and silence / tranquilize

Default: Counter shot   
Shift: Tranquilizing shot   
Alt/Ctrl: Viper Sting (PVP)   

```
#showtooltip
/stopcasting
/stopcasting
/cqs
/cast [mod:shift] Tranquilizing Shot; [mod:alt][mod:ctrl] Viper Sting;[@focus,harm][]Counter Shot
```

# Mouse steady / stealth
Keybind: Mouse DPAD 6   
I put this on my mouse so I can continuously shoot while moving   

In combat: Steady Shot
Otherwise: Camouflage (If talented)   

```
#showtooltip
#/use [mod:shift] Explosive Shot;[talent:2/2]Barrage;#[talent:1/3]A Murder of Crows;[talent:4/3]Chimaera #Shot;
/use [talent:3/3,nocombat]Camouflage;Steady Shot

```